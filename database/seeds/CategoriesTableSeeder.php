<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

         Category::create([
            'title' => 'Anı',
            'parentId' => 0,
            'image' => ''
        ]);

        Category::create([
            'title' => 'Biyografi',
            'parentId' => 0,
            'image' => ''
        ]);

        Category::create([
            'title' => 'Edebiyat',
            'parentId' => 0,
            'image' => ''
        ]);

        Category::create([
            'title' => 'Felsefe',
            'parentId' => 0,
            'image' => ''
        ]);

        Category::create([
            'title' => 'Bilim',
            'parentId' => 0,
            'image' => ''
        ]);

        Category::create([
            'title' => 'Deneme',
            'parentId' => 0,
            'image' => ''
        ]);

    }
}
