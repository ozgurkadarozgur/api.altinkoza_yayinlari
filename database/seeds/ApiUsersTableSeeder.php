<?php

use App\ApiUser;
use Illuminate\Database\Seeder;

class ApiUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ApiUser::create([
            'name' => 'pxr',
            'email' => 'api@pxr.com',
            'password' => bcrypt('123456')
        ]);
    }
}
