<?php

use App\Product;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create([
            'categoryId' => 1,
            'title' => 'Bilinmeyen Bir Kadının Mektubu',
            'description' => 'Bilinmeyen Bir Kadının Mektubu',
            'image' => '',
            'stock' => 100,
            'price' => 50,
            'isActive' => true
        ]);

        Product::create([
            'categoryId' => 1,
            'title' => 'Hayvan Çiftliği',
            'description' => 'Hayvan Çiftliği',
            'image' => '',
            'stock' => 200,
            'price' => 40,
            'isActive' => true
        ]);

    }
}
