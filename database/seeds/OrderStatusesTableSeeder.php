<?php

use App\OrderStatus;
use Illuminate\Database\Seeder;

class OrderStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        OrderStatus::create([
            'title' => 'status1'
        ]);

        OrderStatus::create([
            'title' => 'status2'
        ]);

        OrderStatus::create([
            'title' => 'status3'
        ]);

    }
}
