@extends('layouts.app')

@section('content')

    @if(session()->has('message'))
        <div class="alert alert-success" role="alert">
            {{ session()->get('message') }}
        </div>
    @endif

    <h4>Ürün Ekle</h4>
    <hr />
    <form method="post" action="{{ route('products.store') }}" enctype="multipart/form-data">
        @csrf

        <div class="form-group">
            <label for="title">Ürün Adı</label>
            <input type="text" class="form-control" id="title" name="title" aria-describedby="titleHelp" placeholder="Ürün Adı">
            @if($errors->has('title'))
                <small id="titleHelp" class="form-text text-muted text-danger">{{ $errors->first('title') }}</small>
            @endif
        </div>

        <div class="form-group">
            <label for="image">Fotoğraf</label>
            <input type="file" class="form-control" id="image" name="image" aria-describedby="imageHelp" />
            @if($errors->has('image'))
                <small id="imageHelp" class="form-text text-muted text-danger">{{ $errors->first('image') }}</small>
            @endif
        </div>

        <div class="form-group">
            <label for="categoryId">Kategori</label>
            <select class="form-control" id="categoryId" name="categoryId">
                <option value="0">Seçiniz..</option>
                @foreach($categories as $item)
                    <option value="{{ $item->id }}">{{ $item->title }}</option>
                @endforeach
            </select>
            @if($errors->has('categoryId'))
                <small id="parentIdHelp" class="form-text text-muted">{{ $errors->first('categoryId') }}</small>
            @endif
        </div>

        <div class="form-group">
            <label for="description">Ürün Açıklaması</label>
            <textarea class="form-control" id="description" name="description" aria-describedby="descriptionHelp" placeholder="Ürün Açıklaması"></textarea>
            @if($errors->has('description'))
                <small id="descriptionHelp" class="form-text text-muted">{{ $errors->first('description') }}</small>
            @endif
        </div>

        <div class="form-group">
            <label for="stock">Stok</label>
            <input type="number" class="form-control" id="stock" name="stock" aria-describedby="stockHelp" placeholder="Stok adedi">
            @if($errors->has('stock'))
                <small id="stockHelp" class="form-text text-muted">{{ $errors->first('stock') }}</small>
            @endif
        </div>

        <div class="form-group">
            <label for="stock">Fiyat</label>
            <input type="number" class="form-control" id="price" name="price" aria-describedby="priceHelp" placeholder="Fiyat">
            @if($errors->has('price'))
                <small id="priceHelp" class="form-text text-muted">{{ $errors->first('price') }}</small>
            @endif
        </div>

        <div class="form-check">
            <input type="checkbox" class="form-check-input" id="isActive" name="isActive">
            <label class="form-check-label" for="isActive">Aktif</label>
        </div>

        <button type="submit" class="btn btn-primary">Kaydet</button>
    </form>

@endsection