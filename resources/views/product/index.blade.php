@extends('layouts.app')

@section('content')

    <h4>Ürün Lisesi</h4>
    <hr />
    <a href="{{ route('products.create') }}">Yeni Ekle</a>

    <table class="table table-bordered">
        <thead>
            <th>#</th>
            <th>Ürün Adı</th>
            <th>Kategori Adı</th>
            <th>Resim</th>
        </thead>
        <tbody>
        @foreach($products as $item)
            <tr>
                <td>{{ $item->id }}</td>
                <td>{{ $item->title }}</td>
                <td>{{ $item->category->title }}</td>
                <td><img src="{{ 'http://api.kozayayin.app-xr.com/media/' . $item->image }}" /></td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection