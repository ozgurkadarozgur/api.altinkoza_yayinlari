@extends('layouts.app')

@section('content')

    @if(session()->has('message'))
        <div class="alert alert-success" role="alert">
            {{ session()->get('message') }}
        </div>
    @endif

    <h4>Kategori Ekle</h4>
    <hr />
    <form method="post" action="{{ route('categories.store') }}">
        @csrf
        <div class="form-group">
            <label for="title">Kategori Adı</label>
            <input type="text" class="form-control" id="title" name="title" aria-describedby="titleHelp" placeholder="Kategori Adı">
            <small id="titleHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
        </div>
        <div class="form-group">
            <label for="parentId">Üst Kategori</label>
            <select class="form-control" id="parentId" name="parentId">
                <option value="0">Seçiniz..</option>
                @foreach($categories as $item)
                    <option value="{{ $item->id }}">{{ $item->title }}</option>
                @endforeach
            </select>
            <small id="parentIdHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
        </div>
        <button type="submit" class="btn btn-primary">Kaydet</button>
    </form>

@endsection