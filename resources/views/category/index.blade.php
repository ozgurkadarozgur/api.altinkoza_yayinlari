@extends('layouts.app')

@section('content')

    <h4>Kategori Lisesi</h4>
    <hr />
    <a href="{{ route('categories.create') }}">Yeni Ekle</a>

    <table class="table table-bordered">
        <thead>
            <th>#</th>
            <th>Kategori Adı</th>
            <th>Resim</th>
        </thead>
        <tbody>
            @foreach($categories as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->title }}</td>
                    <td>{{ $item->image }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

@endsection