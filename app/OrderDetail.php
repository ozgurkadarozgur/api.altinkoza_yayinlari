<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $table = 'order_details';

    protected $fillable = [
        'orderId',
        'productId',
        'count',
        'price',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'productId');
    }

}
