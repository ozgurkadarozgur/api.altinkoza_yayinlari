<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;

class ApiUser extends Authenticatable
{
    use HasApiTokens;

    protected $table = 'api_users';

    protected $fillable = [
        'name',
        'email',
        'password',

    ];

    protected $hidden = [
        'password',
    ];

    public function addresses()
    {
        return $this->hasMany(Address::class, 'userId');
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'userId');
    }

}
