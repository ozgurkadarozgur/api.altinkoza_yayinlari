<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'category' => $this->when(request('productId'), new CategoryResource($this->category)),
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'image' => env('APP_URL') . Storage::url($this->image),
            'stock' => $this->stock,
            'price' => $this->price
        ];
    }
}
