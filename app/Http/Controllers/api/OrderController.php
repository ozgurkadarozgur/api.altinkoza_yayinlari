<?php

namespace App\Http\Controllers\api;

use App\Http\Resources\OrderResource;
use App\Order;
use App\Product;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{

    public function index()
    {
        return response()->json([
            'status' => 'success',
            'data' => OrderResource::collection(\request()->user()->orders)
        ]);
    }

    public function show($id)
    {
        $order = Order::find($id);
        if ($order) {
            return response()->json([
                'status' => 'success',
                'data' => new OrderResource($order)
            ]);
        } else {
            return response()->json([
                'status' => 'warning',
                'data' => 'Order not found.'
            ]);
        }

    }

    public function store()
    {
        $validator = Validator::make(\request()->all(),
        [
            'productList' => 'required|json',
        ]);

        if ($validator->fails()){
            return response()->json([
                'status' => 'error',
                'data' => $validator->errors()
            ]);
        } else {

            $productJSON = json_decode(\request('productList'), true);
            $productCollect = collect($productJSON);

            /*
            $totalPrice = 0;
            foreach ($productCollect as $item) {
                $product = Product::find($item["id"]);
                $totalPrice += $product->price * $item["count"];
            }

            $productIdList = $productCollect->pluck('id');

            $productList = Product::find($productIdList);

            $totalPrice = $productList->sum('price');

            */
            $order = Order::create([
                'userId' => \request()->user()->id,
                'orderStatusId' => 1,
                //'totalPrice' => $totalPrice
            ]);

            $order->setDetail($productCollect);

            return response()->json([
                'status' => 'success',
                'data' => new OrderResource($order)
            ]);

        }

    }
}
