<?php

namespace App\Http\Controllers\api;

use App\Category;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\ProductResource;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function index($id)
    {
        $category = Category::find($id);
        if ($category) {
            return response()->json([
                'status' => 'success',
                'data' =>  new CategoryResource($category)
            ]);
        } else {
            return response()->json([
                'status' => 'warning',
                'data' => 'Category not found.'
            ]);
        }
    }

    public function show($id)
    {
        $product = Product::find($id);
        if ($product) {
            return response()->json([
                'status' => 'success',
                'data' =>  new ProductResource($product)
            ]);
        } else {
            return response()->json([
                'status' => 'warning',
                'data' => 'Product not found.'
            ]);
        }

    }

}
