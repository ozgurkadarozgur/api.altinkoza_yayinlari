<?php

namespace App\Http\Controllers\api;

use App\Address;
use Validator;
use App\Http\Resources\AddressResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AddressController extends Controller
{
    public function index()
    {
        return response()->json([
            'status' => 'success',
            'data' => AddressResource::collection(\request()->user()->addresses)
        ]);
    }

    public function store()
    {
        $validator = Validator::make(\request()->all(),
        [
            'title' => 'required',
            'description' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'data' => $validator->errors()
            ]);
        } else {
            $address = Address::create([
                'userId' => \request()->user()->id,
                'title' => \request('title'),
                'description' => \request('description')
            ]);
            return response()->json([
                'status' => 'success',
                'data' => new AddressResource($address)
            ]);
        }

    }

}
