<?php

namespace App\Http\Controllers\api;

use App\ApiUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use \Symfony\Component\HttpFoundation\Request as mRequest;

class ApiUserController extends Controller
{
    public function register()
    {
        $validator = Validator::make(\request()->all(),
            [
                'name' => 'required',
                'email' => 'required',
                'password' => 'required'
            ]);

        if ($validator->fails()){
            return response()->json([
                'status' => 'error',
                'data' => $validator->errors()
            ]);
        } else {
            $apiUser = ApiUser::create([
                'name' => \request('name'),
                'email' => \request('email'),
                'password' => bcrypt(\request('password'))
            ]);
            return response()->json([
                'status' => 'success',
                'data' => $apiUser
            ]);
        }

    }

    public function login()
    {
        $validator = Validator::make(\request()->all(),
            [
                'email' => 'required',
                'password' => 'required'
            ]);

        if ($validator->fails()){
            return response()->json([
                'status' => 'error',
                'data' => $validator->errors()
            ]);
        } else {
            $request = mRequest::create('oauth/token', 'POST', [
                'grant_type' => 'password',
                'client_id' => 2,
                'client_secret' => env('OAUTH_CLIENT_SECRET'),
                'username' => \request('email'),
                'password' => \request('password')
            ]);
            $response = app()->handle($request);
            return $response;
        }
    }

}
