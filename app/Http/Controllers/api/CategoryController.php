<?php

namespace App\Http\Controllers\api;

use App\Category;
use App\Http\Resources\CategoryResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::where('parentId', 0)->get();
        return response()->json([
            'status' => 'success',
            'data' => CategoryResource::collection($categories)
        ]);
    }

    public function children($id)
    {
        $category = Category::find($id);
        if ($category) {
            return response()->json([
                'status' => 'success',
                'data' => $category->children
            ]);
        } else {
            return response()->json([
                'status' => 'warning',
                'data' => 'Category not found.'
            ]);
        }
    }

}
