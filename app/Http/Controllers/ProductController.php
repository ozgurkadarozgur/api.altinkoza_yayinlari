<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('product.create',  compact('categories'));
    }//

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $this->validate(\request(),
            [
                'title' => 'required',
                'categoryId' => 'required',
                'description' => 'required',
                'stock' => 'required',
                'price' => 'required',
            ]
        );

        $product = Product::create([
            'title' => \request('title'),
            'categoryId' => \request('categoryId'),
            'description' => \request('description'),
            'stock' => \request('stock'),
            'price' => \request('price'),
            'isActive' => \request('isActive') ? true : false,
        ]);

        if ($product) {
            $image = Storage::disk('public')->put('/', \request()->file('image'));
            //Storage::disk('sftp')->put('/', \request()->file('image'));
            $product->image = $image;
            $product->save();
        }

        return redirect()->back()->with('message', $product->title . ' başarıyla eklendi.');

    }

    /**
     * Display the specified resource.
     *
     * @param  Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('product.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
