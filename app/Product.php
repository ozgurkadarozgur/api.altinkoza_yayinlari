<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    protected $fillable = [
        'title',
        'categoryId',
        'title',
        'description',
        'image',
        'stock',
        'price',
        'isActive'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class, 'categoryId');
    }

}
