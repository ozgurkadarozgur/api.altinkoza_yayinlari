<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Order extends Model
{
    protected $table = 'orders';

    protected $fillable = [
        'userId',
        'orderStatusId',
        'totalPrice',
    ];

    public function detail()
    {
        return $this->hasMany(OrderDetail::class, 'orderId');
    }

    public function status()
    {
        return $this->belongsTo(OrderStatus::class, 'orderStatusId');
    }

    public function price()
    {
        $totalPrice = 0;
        foreach ($this->detail as $item) {
            $totalPrice += $item->count * $item->price;
        }
        return $totalPrice;
    }

    public function setDetail(Collection $productList)
    {

        foreach ($productList as $item){
            OrderDetail::create([
                'orderId' => $this->id,
                'productId' => $item["id"],
                'count' => $item["count"],
                'price' => Product::find($item["id"])->price
            ]);
        }

    }

}
