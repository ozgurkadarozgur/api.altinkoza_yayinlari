<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $table = 'addresses';

    protected $fillable = [
        'userId',
        'title',
        'description',
    ];

    public function user()
    {
        return $this->belongsTo(ApiUser::class, 'userId');
    }

}
