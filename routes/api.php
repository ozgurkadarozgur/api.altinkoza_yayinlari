<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'auth:api', 'namespace' => 'api'], function (){
    Route::get('addresses', 'AddressController@index');
    Route::post('addresses', 'AddressController@store');

    Route::get('orders', 'OrderController@index');
    Route::post('orders', 'OrderController@store');
    Route::get('orders/{id}', 'OrderController@show');
});

Route::group(['middleware' => 'api', 'namespace' => 'api'], function (){
    Route::post('register', 'ApiUserController@register');
    Route::post('login', 'ApiUserController@login');
    Route::get('categories', 'CategoryController@index');
    Route::get('categories/{categoryId}/children', 'CategoryController@children');

    Route::get('categories/{categoryId}/products', 'ProductController@index');

    Route::get('products/{productId}', 'ProductController@show');

});